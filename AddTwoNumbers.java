public class AddTwoNumbers {

	public static void main(String[] args) {
		ListNode n1 = new ListNode(9);
		ListNode n2 = new ListNode(4, n1);
		ListNode n3 = new ListNode(2, n2);
		ListNode x1 = new ListNode(9);
		ListNode x2 = new ListNode(4, x1);
		ListNode x3 = new ListNode(6, x2);
		ListNode x4 = new ListNode(5, x3);
		System.out.print("l1: ");
		printList(n3);
		System.out.print("l2: ");
		printList(x4);
		ListNode r = addTwoNumbers(n3, x4);
		
		printList(r);
	}

	/**
	 * Definition for singly-linked list. public class ListNode { int val; ListNode
	 * next; ListNode() {} ListNode(int val) { this.val = val; } ListNode(int val,
	 * ListNode next) { this.val = val; this.next = next; } }
	 */
	public static class ListNode {
		int val;
		ListNode next;

		ListNode() {
		}

		ListNode(int val) {
			this.val = val;
		}

		ListNode(int val, ListNode next) {
			this.val = val;
			this.next = next;
		}
	}

	public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
		ListNode result = null;
        ListNode current1 = l1;
        ListNode current2 = l2;
        ListNode next1 = null, next2 = null;
        int remainder = 0;
        while(current1 != null || current2 != null)
        {
        	if(current1 != null)
        	    next1 = current1.next;
            if(current2 != null)
        	    next2 = current2.next;
            int sum = 0;
            if(current2 == null)
                sum = current1.val + 0;
            else if(current1 == null)
                sum = 0 + current2.val;
            else
        	    sum = current1.val + current2.val;
        	if(remainder > 0)
        		sum++;
        	if(sum >= 10)
        	{
        		sum = sum % 10;
        		remainder = 1;
        	}
        	else
        		remainder = 0;
        	if(result != null)
        		result = new ListNode(sum,result);
        	else
        		result = new ListNode(sum);
        	current1 = next1 ;
        	current2 = next2 ;
        }
        if(remainder > 0)
        	result = new ListNode(1,result);
        return reverse(result);
    }

	public static ListNode reverse(ListNode l) {
	    ListNode prev = null;
	    ListNode current = l;
	    ListNode nextNode;

	    while (current != null) {
	        nextNode = current.next; // Store the next node
	        current.next = prev;     // Reverse the pointer
	        prev = current;          // Move prev to current node
	        current = nextNode;      // Move current to next node
	    }

	    return prev; // Return the new head of the reversed list
	}
	public static void printList(ListNode head) {
	    ListNode current = head;
	    while (current != null) {
	        System.out.print(current.val + " ");
	        current = current.next;
	    }
	    System.out.println();
	}


}
