import java.util.Scanner;
import java.util.Stack;

public class ValidParentheses {

	public static void main(String[] args) {
		
        Scanner scan = new Scanner(System.in);
        while(true)
        {
        	boolean isValid = true; 
        	System.out.println("Enter a string only have brackets: (Press 0 to exit)");
        	String input = scan.nextLine();
        	if(input.equals("0"))
        		break;
        	isValid = isValid(input);
        	
        	System.out.println("input: " + input);
        	System.out.println("Is valid: " + isValid);        
        	
        	
        }
        scan.close();

	}
	public static boolean isValid(String s) {
    	final char[] closeBracket = {']', ')', '}'};
    	final char[] openBracket = {'[', '(', '{'};
    	final Stack<Character> stack = new Stack<Character>();
    	for(int i = 0; i < s.length(); i++)
    	{
    		final char c = s.charAt(i);
    		if(c == '[' || c == '(' || c == '{')
    		{
    			stack.push(c);        		
    		}
    		else {
    			if(stack.isEmpty())
    			{
    				return false;
    			}
    			else {
    				char top = stack.pop();
    				int indexOpen = new String(openBracket).indexOf(top);
    				int indexClose = new String(closeBracket).indexOf(c);
    				if(indexOpen != indexClose)
    				{
    					return false;
    				}
    				
    			}
    		}
    	}
    	
    	return stack.isEmpty();     
    }

}
